FROM ubuntu as builder
ENV NGINX_VERSION 1.13.8

WORKDIR /app
RUN apt-get update && apt-get install -y \
  apt-utils \
  autoconf \
  automake \
  build-essential \
  curl \
  git \
  libcurl4-openssl-dev \
  libgeoip-dev \
  liblmdb-dev \
  libpcre++-dev \
  libtool \
  libxml2-dev \
  libyajl-dev \
  pkgconf \
  zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*


RUN git clone --depth 1 -b v3/master --single-branch https://github.com/SpiderLabs/ModSecurity
RUN cd ModSecurity && git submodule init && git submodule update && ./build.sh && ./configure && make && make install

RUN git clone --depth 1 https://github.com/SpiderLabs/ModSecurity-nginx.git
RUN curl -L http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz -o nginx-$NGINX_VERSION.tar.gz && tar zxvf nginx-$NGINX_VERSION.tar.gz && rm nginx-$NGINX_VERSION.tar.gz

RUN mkdir -p /etc/nginx/modules
RUN cd nginx-$NGINX_VERSION && ./configure --with-compat --add-dynamic-module=../ModSecurity-nginx && make modules && cp objs/ngx_http_modsecurity_module.so /etc/nginx/modules

FROM ubuntu
ENV NGINX_VERSION 1.13.8

RUN apt-get update && apt-get install -y \
  curl \
  libcurl4-openssl-dev \
  libgeoip-dev \
  liblmdb-dev \
  libpcre++-dev \
  libtool \
  libxml2-dev \
  libyajl-dev \
  zlib1g-dev \
  && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/modsecurity /usr/local/modsecurity

RUN mkdir -p /etc/nginx/modules
COPY --from=builder /app/nginx-$NGINX_VERSION/objs/ngx_http_modsecurity_module.so /etc/nginx/modules/ngx_http_modsecurity_module.so

RUN curl -L http://nginx.org/keys/nginx_signing.key | apt-key add -
RUN echo "deb http://nginx.org/packages/mainline/ubuntu/ xenial nginx" >> /etc/apt/sources.list
RUN apt-get update && apt-get install \
  nginx=$NGINX_VERSION-1~xenial \
  && rm -rf /var/lib/apt/lists/*

COPY conf/nginx.conf /etc/nginx/nginx.conf
RUN mkdir -p /etc/nginx/modsec
COPY conf/modsec/modsecurity.conf /etc/nginx/modsec/modsecurity.conf
RUN sed -i 's/SecRuleEngine DetectionOnly/SecRuleEngine On/' /etc/nginx/modsec/modsecurity.conf
COPY conf/modsec/main.conf /etc/nginx/modsec/main.conf
COPY conf/conf.d/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80 443
ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]
